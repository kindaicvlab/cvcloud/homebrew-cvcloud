# typed: false
# frozen_string_literal: true

# This file was generated by GoReleaser. DO NOT EDIT.
class Cvcloud < Formula
  desc "client tool to use cvcloud"
  homepage "https://gitlab.com/kindaicvlab/cvcloud/cvcloud"
  version "1.8.7"

  on_macos do
    if Hardware::CPU.arm?
      url "https://gitlab.com/kindaicvlab/cvcloud/cvcloud/-/releases/v1.8.7/downloads/cvcloud_1.8.7_darwin_arm64.tar.gz"
      sha256 "7c37e45fcad921b76c81140fb9bb19bcb423c11cb78cc2880530778d9ed8ba49"

      def install
        bin.install "cvcloud"
      end
    end
    if Hardware::CPU.intel?
      url "https://gitlab.com/kindaicvlab/cvcloud/cvcloud/-/releases/v1.8.7/downloads/cvcloud_1.8.7_darwin_amd64.tar.gz"
      sha256 "e0b3055734611f9af1b59d67ff5ac7eebb993c54ee812c1a6b78d97361b98a04"

      def install
        bin.install "cvcloud"
      end
    end
  end

  on_linux do
    if Hardware::CPU.intel?
      url "https://gitlab.com/kindaicvlab/cvcloud/cvcloud/-/releases/v1.8.7/downloads/cvcloud_1.8.7_linux_amd64.tar.gz"
      sha256 "39dc8f09b7682272fb71b52fb300793b864561ef36d7477d06058fe0d8e5add9"

      def install
        bin.install "cvcloud"
      end
    end
    if Hardware::CPU.arm? && !Hardware::CPU.is_64_bit?
      url "https://gitlab.com/kindaicvlab/cvcloud/cvcloud/-/releases/v1.8.7/downloads/cvcloud_1.8.7_linux_arm.tar.gz"
      sha256 "e9445bda7352a07007b34bd0d953a83aef3be2f7ed1b27af73da502275980da2"

      def install
        bin.install "cvcloud"
      end
    end
    if Hardware::CPU.arm? && Hardware::CPU.is_64_bit?
      url "https://gitlab.com/kindaicvlab/cvcloud/cvcloud/-/releases/v1.8.7/downloads/cvcloud_1.8.7_linux_arm64.tar.gz"
      sha256 "0f71b60487cd39eb9061e84dbf5509249df6536d2dbc1b27d4af0e76dd2aa6a7"

      def install
        bin.install "cvcloud"
      end
    end
  end
end
